import { css } from "reshadow/macro"

export const grid = css`
  grid {
    display: grid;
    grid-template-columns: 8fr 5fr;
    grid-gap: 16px;
    align-content: start;
  }
`
