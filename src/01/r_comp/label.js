import { css } from "reshadow/macro"

export const label = css`
  label {
    font-weight: 500;
    opacity: 0.6;
    display: grid;
    grid-gap: 8px;
  }
`
