import { css } from "reshadow/macro"

export const title_page = css`
  title_page {
    margin: 0;
    color: rgb(var(--main));
    font-size: 32px;
    line-height: 48px;
    font-weight: 300;
  }
`
