import { css } from "reshadow/macro"

export const label = css`
  label {
    display: grid;
    grid-gap: 8px;
    font-size: 14px;
    color: rgba(var(--main), 0.6);
  }
`
