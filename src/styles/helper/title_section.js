import { css } from "reshadow/macro"

export const title_section = css`
  title_section {
    margin: 0;
    font-size: 24px;
    line-height: 32px;
    font-weight: normal;
    color: rgb(var(--main));
  }
`
