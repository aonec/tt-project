import { createContext } from "react"

export const GlobalStoreContext = createContext()
